<b>大雾天安全驾驶技巧与注意事项</b>

<font color=#dd1100>出发前做好检查工作</font>

未雨绸缪，才能决胜于千里之外。要在雾天里驾车出行，我们有必要在出发前检查好车辆状况，尤其做到：将挡风玻璃、车头灯和尾灯擦拭干净，检查车辆灯光、雨刮器、制动等安全设施是否齐全有效。另外，在车内一定要携带三角警示牌或其他警示标志，遇到突发故障停车检修时，要在车前后远于100米的距离处摆放警示牌，提醒其他车辆注意。

<font color=#dd1100>行车时开灯要齐全</font>

准备上路时，我们要打开前后雾灯、示宽灯、近光灯和尾灯，上高速行驶时还要记得开应急灯（双闪），利用灯光来提高能见度。在灯组的构造方面，氙灯由于色温值高，所以穿透能力比较差，所以车辆配有氙灯的车主不要认为开了平日里光亮异常的大灯就万事大吉了，记得一定要开雾灯！如果灯组中能带有透镜、LED，聚拢光线和增加亮度都是对雾天行车有好处的。

<font color=#dd1100>“慢”字当头</font>

这是对付驾车时潜在危险的诀窍，车辆之间、车辆与行人之间都要保持充分的安全距离。根据人的反应能力，当能见度小于500米大于200米时，时速不得超过80公里；能见度小于200米大于100米时，时速不得超过60公里；能见度小于100米大于50米时，时速不得超过40公里；能见度在30米以内时，时速应控制在20公里以下；一般视距10米左右时，时速控制在5公里以下。当遇大雾，能见度极低的时候，建议您最好尽快想办法停车，把车开到路边安全地带或停车场，待大雾散去或能见度改善时再继续前进。

<font color=#dd1100>靠右行驶少走路中央</font>

浓雾行车应该靠右行驶，以公路右侧的行道树、护栏、街沿等为参照物，尽量不要走路中央，有条件的车辆走高速和三环的应开启GPS，它可以提醒你应该在哪个路口出去。雾天在环路或者高速路发生堵车时请尽量不要待在车上，驾乘人员必须下车，迅速离开公路，翻过路边护栏，在道外等候，避免后面车辆追尾造成人员伤亡。

<font color=#dd1100>视线不好勤用灯光和喇叭</font>

雾天行车应开启前后雾灯、示宽灯、近光灯和尾灯，用近光。雾大或走高速应开启应急灯，利用灯光来提高能见度。需要特别注意的是，雾天行车不要使用远光灯，因为远光灯射出的光线容易被雾气反射，会在车前形成白茫茫一片，开车的人反而什么都看不见。在雾天视线不好的情况下，勤按喇叭可以起到警告行人和其他车辆的作用。当听到其他车的喇叭声时，应当立刻鸣笛回应，提示出自己的行车位置。两车交会时应按喇叭提醒对面车辆注意，同时关闭雾灯，注意减速。不过，在市区内要慎用喇叭。

<font color=#dd1100>切忌盲目超车</font>

如果发现前方车辆停靠在右边，不可盲目绕行，要考虑到此车是否在等让对面来车。超越路边停放的车辆时，要在确认其没有起步的意图而对面又无来车后，适时按喇叭，从左侧低速绕过。另外，也请注意小心盯住路中的分道线，不能轧线行驶，否则会有与对向的车相撞的危险。在弯道和坡路行驶时，应提前减速，要避免中途变速、停车或熄火。

<font color=#dd1100>不要急刹车</font>

冬天浓雾会使路面上形成薄霜或薄冰，极易产生侧滑，切不可急打方向盘、猛踏或快松加速踏板，以防侧滑。雾天出行，受尽快冲出浓雾包围急切心理的支配，会无意中提高车速，要注意减速，尽量把车速控制在能及时停车的范围内。若后车车距太近，可轻点几下刹车，使得刹车灯亮起，以便后车注意。

<font color=#dd1100>遇到事故100米外设警示牌</font>

雾天遇到突发故障停车检修或是交通事故时，应在车前后100米外处摆放警示牌，并及时通知交通管理部门，开启视宽灯及应急灯，有随车应急用照明设施的应开启应急照明向后方照射以提醒后面的车辆，并在车前、后方设置反光标志，人员应该立即离开公路并站在较高处等候交警到达现场。