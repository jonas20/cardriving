//
//  CAGFavorite.m
//  ChaseAGirl
//
//  Created by jonas hu on 12-12-11.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "CAGFavorite.h"

@implementation CAGFavorite

@synthesize idFavotite;
@synthesize name;
@synthesize dateTime;


-(id)initWith:(NSInteger )index favorite:(NSArray *)favorite
{
    
    self = [super init];
    if(self)
    {
        NSDictionary *message=(NSDictionary *)[favorite objectAtIndex:index];
        
        
        [self setIdFavotite:[message objectForKey:@"id"]];
        [self setName:[message objectForKey:@"name"]];
        [self setDateTime:[message objectForKey:@"datetime"]];
        
    }
    
    return self;
}



@end
