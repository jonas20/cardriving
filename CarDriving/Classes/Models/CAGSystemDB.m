//
//  CAGSystemDB.m
//  ChaseAGirl
//
//  Created by jonas hu on 12-12-11.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "CAGSystemDB.h"
#import "SingleDate.h"

//获取单例模型的实例
static SingleDate *instance;
static FMDatabase *db;


@implementation CAGSystemDB



+(void)CreateFavorite
{
    instance = [SingleDate getInstance];
    //读取数据库
    db = [instance loadDB];
    
    NSString *sql=[[NSString alloc] initWithFormat:@"CREATE TABLE IF NOT EXISTS Favorite (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT,datetime TEXT)"];
    
    [db executeUpdate:sql];
}

+(void)AddFavorite:(NSString *)Name
{
    NSDate *date=[NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSString *dateString=[dateFormatter stringFromDate:date];
    
    NSString *sql=[[NSString alloc] initWithFormat:@"INSERT INTO Favorite(name,datetime) VALUES('%@','%@')",Name,dateString];
    
    
    [db executeUpdate:sql];
}

+(void)DeleteFavorite:(NSString *)Name//删除收藏
{
    NSString *sql=[[NSString alloc] initWithFormat:@"DELETE FROM Favorite WHERE name='%@'",Name];
    
    
    instance = [SingleDate getInstance];
    //读取数据库
    db = [instance loadDB];
    
    [db executeUpdate:sql];
}



+(void)DeleteFavoriteID:(NSString *)IDFavorite//删除收藏ID
{
    NSString *sql=[[NSString alloc] initWithFormat:@"DELETE FROM Favorite WHERE id='%@'",IDFavorite];
    
    
    instance = [SingleDate getInstance];
    //读取数据库
    db = [instance loadDB];
    
    [db executeUpdate:sql];
}

//判断是否收藏过
+(BOOL)ISFavoriteTable:(NSString *)Name
{
    //判断是不是存在记录数
    NSString *sql=[[NSString alloc] initWithFormat:@"SELECT count(*) AS CT FROM Favorite WHERE name='%@'",Name];
    
    //结果集
    FMResultSet *rs = nil;
    
    rs=[db executeQuery:sql];
    
    while ([rs next]) {
        
        if ([[rs stringForColumn:@"CT"] isEqual:@"0"]) {
            
            return NO;
            
        }
        
    }
    return YES;
    
}


+(NSMutableArray *)AllFavorite
{
    
    
    instance = [SingleDate getInstance];
    //读取数据库
    db = [instance loadDB];
    //结果集
    FMResultSet *rs = nil;
    
    NSString *sql=[[NSString alloc] initWithFormat:@"SELECT id,name,datetime FROM Favorite ORDER BY ID DESC limit 50"];
    
    rs=[db executeQuery:sql];
    
    NSMutableArray *arrayFavorite = [[NSMutableArray alloc] init];
    
    
    while ([rs next])
    {
        //NSLog(@"%@",[[rs resultDict] description]);
        [arrayFavorite addObject:[rs resultDict]];
    }
    //用完以后一定要关闭
    [rs close];
    
    return arrayFavorite;
}

@end
