//
//  CAGSystemDB.h
//  ChaseAGirl
//
//  Created by jonas hu on 12-12-11.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CAGSystemDB : NSObject


+(void)CreateFavorite;//创建收藏表
+(void)AddFavorite:(NSString *)Name;//添加收藏
+(BOOL)ISFavoriteTable:(NSString *)Name;//判断是否收藏过
+(void)DeleteFavorite:(NSString *)Name;//删除收藏
+(void)DeleteFavoriteID:(NSString *)IDFavorite;//删除收藏ID
+(NSMutableArray *)AllFavorite;


@end
