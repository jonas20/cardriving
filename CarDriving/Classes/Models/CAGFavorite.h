//
//  CAGFavorite.h
//  ChaseAGirl
//
//  Created by jonas hu on 12-12-11.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CAGFavorite : NSObject


-(id)initWith:(NSInteger )index favorite:(NSArray *)favorite;

@property(nonatomic,copy)NSString *idFavotite;
@property(nonatomic,copy)NSString *name;
@property(nonatomic,copy)NSString *dateTime;

@end
