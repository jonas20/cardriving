//
//  SingleDate.h
//  SQL
//
//  Created by  on 12-3-22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

//#import <Three20/Three20.h>
#import "FMDatabase.h"

@interface SingleDate : NSObject
{

}
//获取单例对象
+(SingleDate *)getInstance;
//读取数据库
-(FMDatabase *)loadDB;
@end
