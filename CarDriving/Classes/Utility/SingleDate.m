//
//  SingleDate.m
//  SQL
//
//  Created by  on 12-3-22.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SingleDate.h"

#define DATABASE_NAME @"cardriving.sqlite"

@implementation SingleDate

static SingleDate *instance = nil;

//获取单例
+(SingleDate *)getInstance
{
    @synchronized(self) 
    {
        if (instance == nil) 
        {
            instance = [[self alloc] init];
        }
    }
    return instance;
}

//读取数据库
-(FMDatabase* )loadDB
{
    //获取当前程序路径
    NSURL *appUrl = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSString *dbPath = [[appUrl path] stringByAppendingPathComponent:DATABASE_NAME];
    //注释1
    FMDatabase *db= [FMDatabase databaseWithPath:dbPath] ;  
    
    if (![db open]) {  
        NSLog(@"数据库未能创建/打开");  
        return nil;
    }    

    return db;
}
@end
