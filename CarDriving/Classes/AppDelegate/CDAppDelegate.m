//
//  CDAppDelegate.m
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "CDAppDelegate.h"
#import "CDIndexTableViewController.h"
#import "CDFoundation.h"
#import "WapsOffer/AppConnect.h"
#import "CAGSystemDB.h"

@implementation CDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (isFreeVersion) {
        
        [WapsLog setLogThreshold:LOG_ALL]; //开启后台打印调试信息
        
        //******万普广告
        [AppConnect getConnect:@"852501bb9e0eefd879619811820272f2"]; //不指定pid
        
        //初始化插屏广告
        //[AppConnect initPopAd];
        
    }

    //收藏表
    [CAGSystemDB CreateFavorite];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    //CDIndexViewController *indexViewController=[[CDIndexViewController alloc] init];
    
    CDIndexTableViewController *indexViewController=[[CDIndexTableViewController alloc] init];
    
    

    self.mainNavigationController=[[UINavigationController alloc] initWithRootViewController:indexViewController];
    
    //[[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];CFBundleDisplayName
    
    self.mainNavigationController.navigationBar.barStyle = UIBarStyleBlack;

    
    UIImage *navigationBarBackgroundImage =[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"juchi" ofType:@"png"]];

    [self.mainNavigationController.navigationBar setBackgroundImage:navigationBarBackgroundImage forBarMetrics:UIBarMetricsDefault];

    
    self.window.rootViewController=self.mainNavigationController;
    
    [self.window makeKeyAndVisible];
    
    

    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
