//
//  CDAppDelegate.h
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic) UINavigationController *mainNavigationController;

@end
