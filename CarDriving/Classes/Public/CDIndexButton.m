//
//  CDIndexButton.m
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "CDIndexButton.h"
#import "CDFoundation.h"

@implementation CDIndexButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor=textBackgroundColor;
        
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    }
    
    return self;
}

-(id)initWithFrameText:(CGRect)frame LTitle:(NSString *)LTitle
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        [self setTitle:LTitle forState:UIControlStateNormal];
        self.backgroundColor=textBackgroundColor;
        
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self setTitleColor:textanBackgroundColor forState:UIControlStateHighlighted];
        
    }
    
    return self;
}





/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
