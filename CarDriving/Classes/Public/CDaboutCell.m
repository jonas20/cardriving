//
//  PGaboutCell.m
//  PhotoGraphy
//
//  Created by jonas hu on 12-9-28.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "CDaboutCell.h"
#import "CDFoundation.h"

@implementation CDaboutCell

@synthesize leftImageView;
@synthesize lefLable;
@synthesize rightLable;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor=[UIColor whiteColor];
        
        leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10,9, 25, 25)];
        [self.contentView addSubview:leftImageView];

        
        lefLable = [[UILabel alloc] initWithFrame:CGRectMake(43, 12, 200, 20)];
        lefLable.textColor = [UIColor blackColor];
        lefLable.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:lefLable];
        
        
        CGFloat rightLablex=140;
        if ([[UIDevice currentDevice] userInterfaceIdiom ]==UIUserInterfaceIdiomPad) {
            rightLablex=510;
        }
        
        rightLable = [[UILabel alloc] initWithFrame:CGRectMake(rightLablex, 11, 150, 20)];
        rightLable.textColor = Color_blackColor;
        rightLable.font=[UIFont systemFontOfSize:13.0f];
        rightLable.backgroundColor = [UIColor clearColor];
        rightLable.textAlignment=UITextAlignmentRight;
        [self.contentView addSubview:rightLable];
        
        
        

    }
    return self;
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
