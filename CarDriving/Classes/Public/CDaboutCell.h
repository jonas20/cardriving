//
//  PGaboutCell.h
//  PhotoGraphy
//
//  Created by jonas hu on 12-9-28.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDaboutCell : UITableViewCell

@property(nonatomic,strong)UIImageView  *leftImageView;
@property(nonatomic,strong)UILabel      *lefLable;
@property(nonatomic,strong)UILabel      *rightLable;



@end
