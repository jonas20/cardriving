//
//  CDPhotoViewController.m
//  CarDriving
//
//  Created by jonas hu on 13-5-31.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "CDPhotoViewController.h"
#import "CDFoundation.h"

@implementation CDPhotoViewController


#pragma -mark XLCycleScrollView 代理

- (NSInteger)numberOfPages
{
    return 38;
}

- (UIView *)pageAtIndex:(NSInteger)index
{
    if (index < 0)
    {
        return nil;
    }
    
    
    NSLog(@"%f",[UIScreen mainScreen].bounds.size.height);
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication]statusBarOrientation];
    
    UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width)];
    
    if (orientation==UIInterfaceOrientationPortrait||orientation==UIInterfaceOrientationPortraitUpsideDown) {
        
        imageView.frame=CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width,ViewHeight);
        
    }
    
    
    imageView.contentMode=UIViewContentModeScaleAspectFit; //缩放不变形
    imageView.image=[self imageFile:index];
    
    
    
    return imageView;
    
}

#pragma -mark 自定义方法


-(UIImage *)imageFile:(NSInteger)index
{
    
    NSString *filename=[[NSString alloc] initWithFormat:@"%d",index+1];
    
    //图片引用标准方式，效率高
    return [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:filename ofType:@"gif"]];
}



#pragma -mark  内存生命周期管理

-(void)backnavigationController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.view.backgroundColor=[UIColor blackColor];
    
    UILabel *titleLable=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    titleLable.backgroundColor=[UIColor clearColor];
    titleLable.text=self.navigationItem.title;
    titleLable.textAlignment=NSTextAlignmentCenter;
    titleLable.textColor=[UIColor darkGrayColor];
    
    self.navigationItem.titleView=titleLable;

    
    
    //添加返回按钮
    UIImage *image=[UIImage imageNamed:@"navback.png"];
    
    
    UIButton *barButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width-8, image.size.height-8)];
    
    
    [barButton setImage:image forState:UIControlStateNormal];
    [barButton addTarget:self action:@selector(backnavigationController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc] initWithCustomView:barButton ];
    
    self.navigationItem.leftBarButtonItem=backItem;
    
    //添加滚动视图
    self.csView = [[XLCycleScrollView alloc] initWithFrame:self.view.bounds];
    
    self.csView.delegate = self;
    self.csView.datasource = self;
    [self.view addSubview:self.csView];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
