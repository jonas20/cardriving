//
//  PGaboutViewController.m
//  PhotoGraphy
//
//  Created by jonas hu on 12-9-17.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "CDAboutViewController.h"
#import "CDFoundation.h"
#import "CDaboutCell.h"
#import "CDMyAppViewController.h"



@implementation CDAboutViewController

@synthesize tableAbout;
@synthesize array;




#pragma -mark tableView代理

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 43;
}

//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return array.count;
    
    //版本号
    //我要评价
    //微博
    //联系方式
    
    /*
     摄影手册给广大摄影爱好者提供了最方便的     
     */
}


//每个Cell展现的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString  *CellIdentifier = @"cell";
    CDaboutCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell)
    {
        cell = [[CDaboutCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//选中Cell后背景
        
        
        if(indexPath.row==1||indexPath.row==3||indexPath.row==4||indexPath.row==5)
        {
            cell.selectionStyle=UITableViewCellSelectionStyleBlue;//选中Cell后背景
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator; //设置右边箭头样式
        }
    }
    
    if (indexPath.row<array.count) {
        cell.lefLable.text=[array objectAtIndex:indexPath.row];
        
    }
    
    //@"版本:",@"我要评价", @"联系方式",@"分享给朋友",@"购买专业版",@"我的全部应用",nil ];
    if(indexPath.row==0)
    {
        UIImage  *verimage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"TB-about" ofType:@"png"]];
        
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
        
        cell.leftImageView.image=verimage;
        
        cell.rightLable.text=version;
        
    }
    
    if(indexPath.row==1)
    {
        
        UIImage  *verimage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"setting_ico_downloadrecommend" ofType:@"png"]];
        
        cell.leftImageView.image=verimage;
        
    }
    if(indexPath.row==2)
    {
        
        UIImage  *verimage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"setting_ico_feedback" ofType:@"png"]];
        
        cell.leftImageView.image=verimage;
        
        cell.rightLable.text=@"alikeke@gmail.com";
        
    }
    
    
    if(indexPath.row==3)
    {
        
        UIImage  *verimage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"dollar" ofType:@"png"]];
        
        cell.leftImageView.image=verimage;
    }
    
    if(indexPath.row==4)
    {
        
        UIImage  *verimage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"about_star" ofType:@"png"]];
        
        cell.leftImageView.image=verimage;
    }
    
    return cell;
}

//选中某一个Cell 要执行的事件
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CDMyAppViewController *viewController=[[CDMyAppViewController alloc] init];
    
    switch (indexPath.row) {
        case 1://评价
            
            if(isFreeVersion)
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreReview]];
            }
            else
            {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreProReview]];
            }
            
            break;
            
        case 3:
            if(isFreeVersion)
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:AppStoreURLPro]];
            break;
            
        case 4:
            viewController.title=@"我的全部应用";
            [self.navigationController pushViewController:viewController animated:YES];
            
            break;
            
        default:
            break;
    }
}

#pragma -mark 生命周期管


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=Color_aboutTableBackground;
    
    
    
    UILabel *titleLable=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
    titleLable.backgroundColor=[UIColor clearColor];
    titleLable.text=@"关于App";
    titleLable.textColor=[UIColor darkGrayColor];
    titleLable.font=[UIFont systemFontOfSize:14.0f];
    
    self.navigationItem.titleView=titleLable;
    
    //添加一个表格
    CGFloat viewWidth=self.view.frame.size.width;
    //CGFloat viewHeight=ViewHeight;
    NSLog(@"%f",ViewHeight);
    tableAbout= [[UITableView alloc] initWithFrame:CGRectMake(0, 10, viewWidth, ViewHeight) style:UITableViewStyleGrouped];
    
    tableAbout.delegate=self;
    
    tableAbout.dataSource=self;
    //tableAbout.separatorColor=[UIColor clearColor];  //表间距背景色
    tableAbout.backgroundColor=Color_aboutTableBackground;
    tableAbout.backgroundView=nil;

    
    //添加表头
    UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableAbout.frame.size.width, 95)];
    
    //添加logo
    UIImageView *imageLogo=[[UIImageView alloc] initWithFrame:CGRectMake((viewWidth-86)/2, 0, 86, 73)];
    
    imageLogo.contentMode=UIViewContentModeScaleAspectFit; //缩放不变形
    imageLogo.backgroundColor=[UIColor clearColor];
    
    //图片引用标准方式，效率高
    imageLogo.image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"logo" ofType:@"png"]];
    
    [headerView addSubview:imageLogo];
    
    
    NSString *textName=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    
    
    
    CGSize size=[textName sizeWithFont:[UIFont systemFontOfSize:14.0f]
                     constrainedToSize:CGSizeMake(self.view.frame.size.width, 1000000) lineBreakMode:UILineBreakModeCharacterWrap];
    
    
    //lable
    UILabel *label =[[UILabel alloc] initWithFrame:CGRectMake((viewWidth-size.width)/2, 75, size.width, 18)];
    label.backgroundColor=[UIColor clearColor];
    label.textColor=[UIColor darkGrayColor];
    label.font=[UIFont systemFontOfSize:14.0f];
    label.text=textName;
    
    
    label.numberOfLines=0; // 行数，0不限制
    label.textAlignment=UITextAlignmentCenter;
    label.lineBreakMode=UILineBreakModeCharacterWrap; //换行方式 字母换行
    
    
    
    [headerView addSubview:label];
    
    
    tableAbout.tableHeaderView=headerView;
    [self.view addSubview:tableAbout];
    
    //lable
    
    UILabel *labelCopyright =[[UILabel alloc] initWithFrame:CGRectMake(0,0, 300, 18)];
    labelCopyright.backgroundColor=[UIColor clearColor];
    labelCopyright.textColor=[UIColor darkGrayColor];
    labelCopyright.font=[UIFont systemFontOfSize:13.0f];
    labelCopyright.text=@"jonas hu Copyright © 2013 版权所有";
    
    labelCopyright.numberOfLines=0; // 行数，0不限制
    labelCopyright.textAlignment=UITextAlignmentCenter;
    labelCopyright.lineBreakMode=UILineBreakModeCharacterWrap; //换行方式 字母换行
    
    
    tableAbout.tableFooterView=labelCopyright;
    //[self.view  addSubview:labelCopyright];
    
    
    //版本: 我要评价  推荐到新浪微博 联系方式
    
    array=[[NSMutableArray alloc] initWithObjects:@"版本:",@"我要评价", @"联系方式",@"购买专业版",@"我的全部应用",nil ];
    
    if(!isFreeVersion)
    {
        [array replaceObjectAtIndex:3 withObject:@"感谢您的购买！"];
    }
    
    
    //添加返回按钮
    UIImage *image=[UIImage imageNamed:@"navback.png"];
    
    
    UIButton *barButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width-8, image.size.height-8)];
    
    
    [barButton setImage:image forState:UIControlStateNormal];
    [barButton addTarget:self action:@selector(backnavigationController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc] initWithCustomView:barButton ];
    
    self.navigationItem.leftBarButtonItem=backItem;
    
    self.navigationItem.hidesBackButton=YES;
}

-(void)backnavigationController
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewDidAppear:(BOOL)animated
{
    [self.tableAbout deselectRowAtIndexPath:self.tableAbout.indexPathForSelectedRow animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.tableAbout = nil;
    self.array=nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
