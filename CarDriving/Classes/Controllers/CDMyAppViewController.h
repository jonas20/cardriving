//
//  CAGMyAppViewController.h
//  ChaseAGirl
//
//  Created by jonas hu on 12-12-14.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"


@interface CDMyAppViewController : UIViewController<MBProgressHUDDelegate>
{
    MBProgressHUD *HUD;

}

@property(nonatomic,strong)NSArray *applistArray;
@end
