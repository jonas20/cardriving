//
//  CDPhotoViewController.h
//  CarDriving
//
//  Created by jonas hu on 13-5-31.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XLCycleScrollView.h"

@interface CDPhotoViewController : UIViewController<XLCycleScrollViewDatasource,XLCycleScrollViewDelegate >


@property(nonatomic,strong)XLCycleScrollView *csView;


@end
