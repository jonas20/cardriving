//
//  CAGFavoritesViewController.m
//  ChaseAGirl
//
//  Created by jonas hu on 12-11-13.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "CAGFavoritesViewController.h"
#import "CAGFavorite.h"
#import "CAGSystemDB.h"
#import "CDTextTableViewController.h"
@implementation CAGFavoritesViewController

//@synthesize tableFavorites;
@synthesize arrayFavorites;



#pragma mark -表格数据源及代理

//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return  arrayFavorites.count;
    
}

//每个Cell展现的内容
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier=@"CellIdentifier";
    
    //重用机制
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier]; //重用Cell，只加载每屏展现的内容框架
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator; //设置右边箭头样式
        cell.selectionStyle=UITableViewCellSelectionStyleGray;//选中Cell后背景
        
        
        //标题
        UILabel *textLable=[[UILabel alloc] initWithFrame:CGRectMake(38, 6, self.view.frame.size.width-20, 0)];
        
        textLable.font=[UIFont systemFontOfSize:14.0f];
        textLable.numberOfLines=0;
        textLable.lineBreakMode=UILineBreakModeCharacterWrap;
        textLable.backgroundColor=[UIColor clearColor];
        textLable.tag=100;
        
        
        //添加时间
        UILabel *addDateLable=[[UILabel alloc] initWithFrame:CGRectMake(38, 26, self.view.frame.size.width-20, 0)];
        
        addDateLable.font=[UIFont systemFontOfSize:9.0f];
        addDateLable.textColor=[UIColor darkGrayColor];
        addDateLable.numberOfLines=0;
        addDateLable.lineBreakMode=UILineBreakModeCharacterWrap;
        addDateLable.backgroundColor=[UIColor clearColor];
        addDateLable.tag=101;
        
        

        [cell.contentView addSubview:textLable];
        [cell.contentView addSubview:addDateLable];

        
    }
    
    if([arrayFavorites count ]>indexPath.row)
    {
        CAGFavorite *favoriteCls=[[CAGFavorite alloc] initWith:indexPath.row favorite:arrayFavorites];
        
        
        CGSize size=[favoriteCls.name sizeWithFont:[UIFont systemFontOfSize:14.0f]
                                constrainedToSize:CGSizeMake(self.view.frame.size.width-20, 1000000) lineBreakMode:UILineBreakModeCharacterWrap];
        
        
        UILabel *label=(UILabel *) [cell.contentView viewWithTag:100];
        label.frame=CGRectMake(38, 6, size.width, size.height);
        label.text=favoriteCls.name;
        
        
        
        //添加时间
        NSString *textAddDate=[[NSString alloc] initWithFormat:@"添加时间:%@",favoriteCls.dateTime];
        
        size=[textAddDate sizeWithFont:[UIFont systemFontOfSize:9.0f]
                     constrainedToSize:CGSizeMake(self.view.frame.size.width-20, 1000000) lineBreakMode:UILineBreakModeCharacterWrap];
        
        
        UILabel *labeladddate=(UILabel *) [cell.contentView viewWithTag:101];
        labeladddate.frame=CGRectMake(38, 26, size.width, size.height);
        labeladddate.text= textAddDate;
         
        
    }
    
    //iocn
//    UIImage  *intImage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"car" ofType:@"png"]];

    cell.imageView.image=[UIImage imageNamed:@"car.png"];
    return cell;
    
}



//选中某一个Cell 要执行的事件
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if([arrayFavorites count ]>indexPath.row)
    {
        
        CAGFavorite *favoriteCLS=[[CAGFavorite alloc] initWith:indexPath.row favorite:arrayFavorites ];
        
        CDTextTableViewController *textTableView=[[CDTextTableViewController alloc] init];
        
        
        textTableView.titleName=favoriteCLS.name;
        
        
        [self.navigationController pushViewController:textTableView animated:YES];
        
        
    }
}



//添加修改状态可见图标
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewCellEditingStyleDelete;
    
}



//添加修改状态事件
-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle==UITableViewCellEditingStyleDelete)
    {
        if([arrayFavorites count]>indexPath.row)
        {
            NSLog(@"%@",arrayFavorites);
        
            CAGFavorite *favoriteCls=[[CAGFavorite alloc] initWith:indexPath.row favorite:arrayFavorites];
                
            [CAGSystemDB DeleteFavoriteID:favoriteCls.idFavotite];
            
            [arrayFavorites removeObjectAtIndex:indexPath.row];
            
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
            
        }
    }
    
}

//每行高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 45;
}

#pragma -mark 自定义方法

//显示修改按钮
-(void) editButtonClicked:(UIButton *) editButton
{
    self.tableView.editing=!self.tableView.editing;
    
    if(self.tableView.editing)
    {
        //显示完成
        [editButton setImage:DoneImage forState:UIControlStateNormal];
        [editButton setImage:DoneImageDown forState:UIControlStateHighlighted];
        
    }
    else {
        
        
        [editButton setImage:editImage forState:UIControlStateNormal];
        [editButton setImage:editImageDown forState:UIControlStateHighlighted];
        
    }
}


-(void)backnavigationController
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma -mark 内存及生命周期管理


-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:YES];
        
    if ([CAGSystemDB AllFavorite].count!=arrayFavorites.count) {
        arrayFavorites=[CAGSystemDB AllFavorite];
        
        [self.tableView reloadData];
    }
    
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //标题
    UILabel *titleLable=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
    titleLable.backgroundColor=[UIColor clearColor];
    titleLable.text=@"我的收藏";
    
    titleLable.textColor=[UIColor blackColor];
    [titleLable setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    
    self.navigationItem.titleView=titleLable;
    
    
    //添加一个表格
//    CGFloat viewWidth=self.view.frame.size.width;
//        
//    tableFavorites= [[UITableView alloc] initWithFrame:CGRectMake(0, 0, viewWidth, self.view.frame.size.height) style:UITableViewStyleGrouped];
//    
//    tableFavorites.delegate=self;
//    
//    tableFavorites.dataSource=self;
    //      _tableView.separatorColor=[UIColor clearColor];  //表间距背景色
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.backgroundView=nil;
    
    //[self.view addSubview:tableFavorites];

    
    arrayFavorites=[CAGSystemDB AllFavorite];
    
//    editImage=[UIImage imageNamed:@"Editbutton.png"];
//    editImageDown=[UIImage imageNamed:@"Editbutton_down.png"];
//    
//    DoneImage=[UIImage imageNamed:@"Publish_icon_OK.png"];
//    DoneImageDown=[UIImage imageNamed:@"Publish_icon_OK_down.png"];
    
    editImage=[UIImage imageNamed:@"feeddetail_delete_btn.png"];
    editImageDown=[UIImage imageNamed:@"feeddetail_delete_btn.png"];
    
    DoneImage=[UIImage imageNamed:@"alert_tick.png"];
    DoneImageDown=[UIImage imageNamed:@"alert_tick.png"];
    
    
    //添加修改button

    UIButton *editButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, editImage.size.width, editImage.size.height)];
    
    
    [editButton setImage:editImage forState:UIControlStateNormal];
    [editButton setImage:editImageDown forState:UIControlStateHighlighted];
    
    [editButton addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *editButtonItem=[[UIBarButtonItem alloc] initWithCustomView:editButton];
    self.navigationItem.rightBarButtonItem=editButtonItem;
    
    
    //添加返回按钮
    UIImage *image=[UIImage imageNamed:@"navback.png"];
    
    
    UIButton *barButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width-8, image.size.height-8)];
    
    
    [barButton setImage:image forState:UIControlStateNormal];
    [barButton addTarget:self action:@selector(backnavigationController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc] initWithCustomView:barButton ];
    
    self.navigationItem.leftBarButtonItem=backItem;
    
    self.navigationItem.hidesBackButton=YES;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
//    [self setTableFavorites:nil];
    [self setArrayFavorites:nil];
    // Dispose of any resources that can be recreated.
}

@end
