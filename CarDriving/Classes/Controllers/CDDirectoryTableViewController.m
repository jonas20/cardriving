//
//  CDDirectoryTableViewController.m
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "CDDirectoryTableViewController.h"
#import "CDTextTableViewController.h"
#import "CDFoundation.h"
#import "WapsOffer/AppConnect.h"

@implementation CDDirectoryTableViewController



#pragma -mark alert 代理
-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==[alertView cancelButtonIndex]) {
        
        
    }
    else
    {
        [self pointsWallButtonClick];
        
    }
}

#pragma -mark 万普积分墙回调方法

-(void)getUpdatedPoints:(NSNotification*)notifyObj
{
    WapsUserPoints *userPointsObj = notifyObj.object;
    
    pointsValue=[userPointsObj getPointsValue];
    
}

//打开积分墙
-(void)pointsWallButtonClick
{
    
   // [self HideTabBar:YES];
    [AppConnect showOffers:self];
}

- (void)onOfferClosed:(NSNotification*)notifyObj
{
    [self HideTabBar:NO];
    
    //[self.navigationController setNavigationBarHidden:NO animated:YES];
    
	//NSLog(@"%@", @"Offer已关闭");
}


#pragma -mark 自定义方法


- (void)HideTabBar:(BOOL)hidden{
    
    //    [UIApplication sharedApplication].statusBarHidden = hidden;
    [self.navigationController setNavigationBarHidden:hidden];
    
    [UIView beginAnimations:nil context:NULL];
    
    [UIView setAnimationDuration:0];
    
    CGFloat heightView=[UIScreen mainScreen].bounds.size.height;
    
    
    for(UIView *view in self.tabBarController.view.subviews){
        
        
        if([view isKindOfClass:[UITabBar class]]){   //处理UITabBar视图
            
            if (hidden) {
                
                
                [view setFrame:CGRectMake(view.frame.origin.x,heightView, view.frame.size.width,							view.frame.size.height)];
                
            } else {
                
                [view setFrame:CGRectMake(view.frame.origin.x, heightView-48, view.frame.size.width,					view.frame.size.height)];
                
            }
            
        }else{   //处理其它视图
            
            if (hidden) {
                
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,heightView)];
                
                
            } else {
                
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width,heightView-48)];
                
            }
            
        }
        
    }
    
    [UIView commitAnimations];
    
}



-(void)backnavigationController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];


    UILabel *titleLable=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
    titleLable.backgroundColor=[UIColor clearColor];
    titleLable.text=self.keystring;
    titleLable.textColor=[UIColor darkGrayColor];

    self.navigationItem.titleView=titleLable;
    
    NSString *path=[[NSBundle mainBundle] pathForResource:@"Directory" ofType:@"plist"];
    
    NSDictionary *carDictionary=[[NSDictionary alloc] initWithContentsOfFile:path];
    
    self.arrTitle=[[NSArray alloc] initWithArray:[carDictionary objectForKey:self.keystring]];
    
    
    //添加返回按钮
    UIImage *image=[UIImage imageNamed:@"navback.png"];
    
    
    UIButton *barButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width-8, image.size.height-8)];
    
    
    [barButton setImage:image forState:UIControlStateNormal];
    [barButton addTarget:self action:@selector(backnavigationController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc] initWithCustomView:barButton ];
    
    self.navigationItem.leftBarButtonItem=backItem;
    
    self.navigationItem.hidesBackButton=YES;
    
    
    if (isFreeVersion) {
        
        
        //添加积分墙图标
        
        UIImage  *intImage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"navbar_fav" ofType:@"png"]];
        
        UIButton *intbarButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, intImage.size.width, intImage.size.height)];
        
        
        [intbarButton setImage:intImage forState:UIControlStateNormal];
        
        [intbarButton addTarget:self action:@selector(pointsWallButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *intBarItem=[[UIBarButtonItem alloc] initWithCustomView:intbarButton];
        
        self.navigationItem.rightBarButtonItem=intBarItem;

        
        
        
             
        //万普广告
        
        //指定获取用户积分的回调方法
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(getUpdatedPoints:)
                                                     name:WAPS_GET_POINTS_SUCCESS
                                                   object:nil];
        //获取用户积分
        [AppConnect getPoints];
        
        //获取Offer关闭事件的回调方法
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onOfferClosed:)
                                                     name:WAPS_OFFER_CLOSED
                                                   object:nil];
        
        
        //万普广告 end
    }
    
    //添加表格
    //self.tableViewDirectory=[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, ViewHeight) style:UITableViewStyleGrouped];
    

// self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    
//    self.tableViewDirectory.backgroundView=nil;
//
//    self.tableViewDirectory.dataSource=self;
//    self.tableViewDirectory.delegate=self;
//    
//    [self.view addSubview:self.tableViewDirectory];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

//行数
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    NSLog(@"%lu",(unsigned long)[self.arrTitle count ]);
    return [self.arrTitle count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell==nil) {
        
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        
        cell.selectionStyle=UITableViewCellSelectionStyleBlue;
//        UIImage  *listIconImage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"glyphicons_028_cars" ofType:@"png"]];
//        
//        cell.imageView.image=listIconImage;
//         cell.imageView.frame=CGRectMake(0, 0, 8, 8);

    }
    
    if (self.arrTitle.count>indexPath.row) {
        
        cell.textLabel.font=[UIFont systemFontOfSize:13.5f];
        cell.textLabel.text=self.arrTitle[indexPath.row];
        
                


    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 45.0f;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isFreeVersion&&indexPath.row>3) {
        
        if (pointsValue<90) {
            
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"对不起" message:[[NSString alloc] initWithFormat:@"免费版需要90积分才能阅读，您可以通过积分墙获取积分！当前积分:%d" ,pointsValue] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"积分墙",nil];
            
            [alert show];
            
            return;
        }
        
        [AppConnect spendPoints:2];
        
    }    
    
    
    CDTextTableViewController *textTableView=[[CDTextTableViewController alloc] init];
    
    NSLog(@"%@",self.arrTitle[[indexPath row ]]);
    
    textTableView.titleName=self.arrTitle[[indexPath row ]];
    
    
    [self.navigationController pushViewController:textTableView animated:YES];
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
}

@end
