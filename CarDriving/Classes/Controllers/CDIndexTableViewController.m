//
//  CDIndexTableViewController.m
//  CarDriving
//
//  Created by jonas hu on 13-5-8.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "CDIndexTableViewController.h"
#import "CDFoundation.h"
#import "CDDirectoryTableViewController.h"
#import "CDIndexButton.h"
#import "CDAboutViewController.h"
#import "CDPhotoViewController.h"
#import "CAGFavoritesViewController.h"

@implementation CDIndexTableViewController


#pragma mark - 自定义方法

-(void)onClickAboutView
{
    CDAboutViewController  *cdAboutViewController=[[CDAboutViewController alloc] init];
    
    [self.navigationController pushViewController:cdAboutViewController animated:YES];
}

#pragma mark - 内存生命管理
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    // self.view.frame=CGRectMake(0,0,self.view.frame.size.width,300);
    
    self.view.backgroundColor=viewBackgroundColor;
    
    UILabel *titleLable=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    titleLable.backgroundColor=[UIColor clearColor];
    titleLable.text=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName" ];
    titleLable.textAlignment=NSTextAlignmentCenter;
    
    titleLable.textColor=[UIColor blackColor];
    [titleLable setFont:[UIFont fontWithName:@"Helvetica-Bold" size:18]];
    
    self.navigationItem.titleView=titleLable;
    self.arrayInfo=[[NSArray alloc] initWithObjects:@"新手驾驶",@"雨天驾驶",@"女性驾驶",@"雾天驾驶",@"冰雪驾驶",@"违章解释",@"交通事故全责图解",@"我的收藏",nil];
    
    
    //添加关于
    
    UIImage  *intImage=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"lottery_button_lotteryGuize" ofType:@"png"]];
    
    UIButton *intbarButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20 ,20)];
    
    
    [intbarButton setImage:intImage forState:UIControlStateNormal];
    
    [intbarButton addTarget:self action:@selector(onClickAboutView) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *intBarItem=[[UIBarButtonItem alloc] initWithCustomView:intbarButton];
    
    self.navigationItem.rightBarButtonItem=intBarItem;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier=@"CellIdentifierCate";
    
    //重用机制
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier]; //重用Cell，只加载每屏展现的内容框架
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
        cell.accessoryType=UITableViewCellAccessoryNone; //设置右边箭头样式
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;//选中Cell后背景
    }
    
    //    NSLog(@"%f",ViewHeight);
    
    float buttonHeight=10;
    
    //1
    CDIndexButton *indexButton1=[[CDIndexButton alloc] initWithFrameText:CGRectMake(20, buttonHeight, 128, 128) LTitle:self.arrayInfo[0]];
    
    [indexButton1 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:indexButton1];
    
    
    //2
    CDIndexButton *indexButton2=[[CDIndexButton alloc] initWithFrameText:CGRectMake(172, buttonHeight,128, 128) LTitle:self.arrayInfo[1]];
    
    [indexButton2 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:indexButton2];
    
    
    //3
    CDIndexButton *indexButton3=[[CDIndexButton alloc] initWithFrameText:CGRectMake(20, 138+buttonHeight, 128, 128) LTitle:self.arrayInfo[2]];
    
    [indexButton3 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:indexButton3];
    
    
    //4
    
    CDIndexButton *indexButton4=[[CDIndexButton alloc] initWithFrameText:CGRectMake(172, 138+buttonHeight, 128, 128) LTitle:self.arrayInfo[3]];
    
    [indexButton4 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:indexButton4];
    
    
    //5
    
    CDIndexButton *indexButton5=[[CDIndexButton alloc] initWithFrameText:CGRectMake(20, 276+buttonHeight, 128, 128) LTitle:self.arrayInfo[4]];
    
    [indexButton5 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:indexButton5];
    
    
    //6
    CDIndexButton *indexButton6=[[CDIndexButton alloc] initWithFrameText:CGRectMake(172, 276+buttonHeight, 128, 128) LTitle:self.arrayInfo[5]];
    
    [indexButton6 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:indexButton6];
    
    
    //7
    CDIndexButton *indexButton7=[[CDIndexButton alloc] initWithFrameText:CGRectMake(20, 414+buttonHeight, 128, 128) LTitle:self.arrayInfo[6]];
    
    [indexButton7 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    indexButton7.titleLabel.font=[UIFont systemFontOfSize:15.0f];
    
    [cell addSubview:indexButton7];
    
    
    //8
    CDIndexButton *indexButton8=[[CDIndexButton alloc] initWithFrameText:CGRectMake(172, 414+buttonHeight, 128, 128) LTitle:self.arrayInfo[7]];
    
    [indexButton8 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    indexButton8.titleLabel.font=[UIFont systemFontOfSize:15.0f];
    
    [cell addSubview:indexButton8];
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //return 552;
    
    float buttonHeight=10;
    
    int sum=0;
    if(self.arrayInfo.count%2==0)
    {
        sum=self.arrayInfo.count/2;
    }
    else
    {
        sum=self.arrayInfo.count/2+1;
                
    }
    return (128*sum)+(sum*10)+buttonHeight;

}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


-(void)onClickDirectory:(UIButton *) sender
{
    if ([sender.titleLabel.text isEqualToString:self.arrayInfo[7] ]) {
        
        CAGFavoritesViewController *cagFavoritesViewController=[[CAGFavoritesViewController alloc] init];

        [self.navigationController pushViewController:cagFavoritesViewController animated:YES];
        
        return;
    }
    
    
    
    if ([sender.titleLabel.text isEqualToString:self.arrayInfo[6] ]) {
        
        //滚动视图
        CDPhotoViewController *cdPhotoViewController=[[CDPhotoViewController alloc] init];
        cdPhotoViewController.navigationItem.title=sender.titleLabel.text;

        [self.navigationController pushViewController:cdPhotoViewController animated:YES];
    }
    else
    {
    
        //表格视图
        CDDirectoryTableViewController *directoryTableViewController=[[CDDirectoryTableViewController alloc] init ];
    
        directoryTableViewController.keystring=sender.titleLabel.text;
        [self.navigationController pushViewController:directoryTableViewController animated:YES ];
        
    }
}


@end
