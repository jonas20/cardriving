//
//  CDTextTableViewController.m
//  CarDriving
//
//  Created by jonas hu on 13-5-3.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "CDTextTableViewController.h"
#import "RCViewCell.h"
#import "CAGSystemDB.h"

@implementation CDTextTableViewController




//添加收藏
-(void)favoriteButtonAdd:(UIButton *)sender
{
    [CAGSystemDB AddFavorite:self.titleName];
    
    
    [self.favoriteButton removeTarget:self action:@selector(favoriteButtonAdd:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.favoriteButton setImage:self.favoriteImage forState:UIControlStateNormal];
    [self.favoriteButton setImage:self.favoriteImageno forState:UIControlStateHighlighted];
    [self.favoriteButton addTarget:self action:@selector(favoriteButtonDelete:) forControlEvents:UIControlEventTouchUpInside];
    
    
}



//删除收藏
-(void)favoriteButtonDelete:(UIButton *)sender
{
    [CAGSystemDB DeleteFavorite:self.titleName];
    [self.favoriteButton removeTarget:self action:@selector(favoriteButtonDelete:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.favoriteButton setImage:self.favoriteImageno forState:UIControlStateNormal];
    [self.favoriteButton setImage:self.favoriteImage forState:UIControlStateHighlighted];
    [self.favoriteButton addTarget:self action:@selector(favoriteButtonAdd:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}




-(void)backnavigationController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
       

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *filepath=[[NSBundle mainBundle] pathForResource:self.titleName ofType:@"txt"];
    
    NSString *fileInfo=[NSString stringWithContentsOfFile:filepath encoding:NSUTF8StringEncoding error:nil];
    
    
    self.dataArray = [NSMutableArray array];
    
    NSMutableDictionary *row7 = [NSMutableDictionary dictionary];
    [row7 setObject:[NSString stringWithFormat:@"<font size=14.5>%@</font>",fileInfo] forKey:@"text"];
    
    
    [self.dataArray addObject:row7];
    
    
    
    self.tableView.backgroundColor=[UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;



    UILabel *titleLable=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, 44)];
    titleLable.backgroundColor=[UIColor clearColor];
    titleLable.text=self.titleName;
    titleLable.textColor=[UIColor darkGrayColor];
    titleLable.font=[UIFont systemFontOfSize:13.0f];

    
    self.navigationItem.titleView=titleLable;
    
    
    
    //添加返回按钮
    UIImage *image=[UIImage imageNamed:@"navback.png"];
    
    
    UIButton *barButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width-8, image.size.height-8)];
    
    
    [barButton setImage:image forState:UIControlStateNormal];
    [barButton addTarget:self action:@selector(backnavigationController) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem=[[UIBarButtonItem alloc] initWithCustomView:barButton ];
    
    self.navigationItem.leftBarButtonItem=backItem;
    
    self.navigationItem.hidesBackButton=YES;

    
    //添加收藏按钮
    //添加收藏button
    
    self.favoriteImageno=[UIImage imageNamed:@"feeddetail_unlike_btn@2x.png"];
    self.favoriteImage=[UIImage imageNamed:@"feeddetail_like_btn@2x.png"];
    
    self.favoriteButton=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.favoriteImageno.size.width/2,self.favoriteImageno.size.height/2)];
    
    if ([CAGSystemDB ISFavoriteTable:self.titleName]) {
        
        [self.favoriteButton setImage:self.favoriteImage forState:UIControlStateNormal];
        [self.favoriteButton setImage:self.favoriteImageno forState:UIControlStateHighlighted];
        [self.favoriteButton addTarget:self action:@selector(favoriteButtonDelete:) forControlEvents:UIControlEventTouchUpInside];
        
        
    }
    else
    {
        [self.favoriteButton setImage:self.favoriteImageno forState:UIControlStateNormal];
        [self.favoriteButton setImage:self.favoriteImage forState:UIControlStateHighlighted];
        [self.favoriteButton addTarget:self action:@selector(favoriteButtonAdd:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    UIBarButtonItem *favoriteButtonItem=[[UIBarButtonItem alloc] initWithCustomView:self.favoriteButton];
    
    self.navigationItem.rightBarButtonItem=favoriteButtonItem;

    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
	NSMutableDictionary *rowInfo = [self.dataArray objectAtIndex:indexPath.row];
	if ([rowInfo objectForKey:@"cell_height"])
	{
		return [[rowInfo objectForKey:@"cell_height"] floatValue];
	}
	else
	{
        
        //NSString *plainData = [RTLabel getParsedPlainText:[rowInfo objectForKey:@"text"]];
        
        
        RCLabel *tempLabel = [[RCLabel alloc] initWithFrame:CGRectMake(10,0,300,100)];
        //tempLabel.lineBreakMode = kCTLineBreakByTruncatingTail;
        RTLabelComponentsStructure *componentsDS = [RCLabel extractTextStyle:[rowInfo objectForKey:@"text"]];
        tempLabel.componentsAndPlainText = componentsDS;
        
        
        CGSize optimalSize = [tempLabel optimumSize];

        [rowInfo setObject:[NSNumber numberWithFloat:optimalSize.height + 5] forKey:@"cell_height"];
		return [[rowInfo objectForKey:@"cell_height"] floatValue];
	}
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataArray count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    static NSString *CellIdentifier = @"DemoTableViewCell";
    NSMutableDictionary *rowInfo = [self.dataArray objectAtIndex:indexPath.row];
    
    RCViewCell *cell = (RCViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
	{
        cell.backgroundColor=[UIColor clearColor];
        cell = [[RCViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    RTLabelComponentsStructure *componentsDS = [RCLabel extractTextStyle:[rowInfo objectForKey:@"text"]];
    cell.rtLabel.componentsAndPlainText = componentsDS;
    
    return cell;
}

@end
