//
//  CDTextTableViewController.h
//  CarDriving
//
//  Created by jonas hu on 13-5-3.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingleDate.h"

@interface CDTextTableViewController : UITableViewController
{
    
    //获取单例模型的实例
    SingleDate *instance;
    //数据库对象
    FMDatabase *db;
}

@property(nonatomic,copy)  NSString *titleName;
@property(nonatomic,strong)NSMutableArray *dataArray;

@property(nonatomic,strong ) UIImage *favoriteImage;
@property(nonatomic,strong ) UIImage *favoriteImageno;
@property(nonatomic,strong)UIButton *favoriteButton;
@end
