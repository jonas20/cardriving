//
//  PGaboutViewController.h
//  PhotoGraphy
//
//  Created by jonas hu on 12-9-17.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CDAboutViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>


@property(nonatomic,strong)UITableView         *tableAbout;
@property(nonatomic,strong)NSMutableArray      *array;


@end


