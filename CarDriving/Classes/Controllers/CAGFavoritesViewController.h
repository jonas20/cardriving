//
//  CAGFavoritesViewController.h
//  ChaseAGirl
//
//  Created by jonas hu on 12-11-13.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SingleDate.h"

@interface CAGFavoritesViewController : UITableViewController
{
    //获取单例模型的实例
    SingleDate *instance;
    //数据库对象
    FMDatabase *db;
    
    
    UIImage *editImage;
    UIImage *editImageDown;
    
    UIImage *DoneImage;
    UIImage *DoneImageDown;
    
    
}

//@property (nonatomic,strong)UITableView *tableFavorites;
@property (nonatomic,strong)NSMutableArray *arrayFavorites;




@end
