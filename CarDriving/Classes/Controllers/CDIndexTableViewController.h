//
//  CDIndexTableViewController.h
//  CarDriving
//
//  Created by jonas hu on 13-5-8.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDIndexTableViewController : UITableViewController

@property(nonatomic,strong)NSArray *arrayInfo;

@end
