//
//  CAGMyAppViewController.m
//  ChaseAGirl
//
//  Created by jonas hu on 12-12-14.
//  Copyright (c) 2012年 jonas hu. All rights reserved.
//

#import "CDMyAppViewController.h"
#import "CDFoundation.h"
#import "CDMyAppButton.h"
#import "ASIHTTPRequest.h"


@implementation CDMyAppViewController

@synthesize applistArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)backnavigationController
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)appButtonClick:(CDMyAppButton *)sender
{
    
//    NSLog(@"%@",sender.appInfo[2]);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sender.appInfo[2]]];


}


//下载PLIST 文件
-(void)DownloadBookList
{
    
    NSString *path=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/myapplist.plist"];
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"http://jonashubook.googlecode.com/svn/trunk/myapp/myapplist.xml"] ];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDownloadDestinationPath:path];
    
    [request startSynchronous ];
    
    
    NSError *error = [request error];
    if (error) {
        
        NSLog(@"%@",error.description);
    }
    
    applistArray=[[NSArray alloc] initWithContentsOfFile:path];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    UILabel *titleLable=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 44)];
    titleLable.backgroundColor=[UIColor clearColor];
    titleLable.text=@"我的全部应用";
    titleLable.textColor=[UIColor darkGrayColor];
    titleLable.font=[UIFont systemFontOfSize:14.0f];
    
    self.navigationItem.titleView=titleLable;
    
    
    self.view.backgroundColor=Color_BackgroundColor;

    
    
	HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
	[self.navigationController.view addSubview:HUD];
	
	HUD.delegate = self;
	HUD.labelText = @"请稍等...";
    
    //显示对话框
    [HUD showAnimated:YES whileExecutingBlock:^{
        
        [self DownloadBookList];

        [self performSelectorOnMainThread:@selector(addUIScrollView) withObject:nil waitUntilDone:YES];

        
    } completionBlock:^{
        
        //操作执行完后取消对话框
        [HUD removeFromSuperview];
        HUD = nil;
        
    }];
}

-(void)addUIScrollView
{
    
    UIScrollView *scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height)];
    
    scrollView.pagingEnabled=NO;
    
    
    [self.view addSubview:scrollView];
    
    //内容视图
    UIView *contentView=[[UIView alloc] initWithFrame:CGRectMake(25, 25, 270, self.view.bounds.size.height) ];
    
    for (int i=0; i<applistArray.count; i++) {
        
        int startX=0;
        int startY=0;
        
        startY=(i/3)*110;
        
        switch (i%3) {
                
            case 0:
                startX=0;
                break;
            case 1:
                startX=105;
                break;
            case 2:
                startX=207;
                break;
                
            default:
                break;
        }
        
        NSArray *array=[[NSArray alloc] initWithArray:applistArray[i]];
        
        if(array.count>0)
        {
            
            NSData *imageData=[[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:array[1]]];
            
            UIImage *applogoImage=[UIImage imageWithData:imageData];
            
            // UIButton *appButton=[[UIButton alloc] initWithFrame:CGRectMake(startX, startY, 60, 60)];
            
            CDMyAppButton *appButton=[[CDMyAppButton alloc] initWithFrame:CGRectMake(startX, startY, 60, 60)];
            
            appButton.appInfo=array;
            
            [appButton setImage:applogoImage forState:UIControlStateNormal];
            
            
            [appButton  addTarget:self action:@selector(appButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            
            
            UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(startX-14, startY+62, 90, 20)];
            label.text=array[0];
            label.backgroundColor=[UIColor clearColor];
            label.font=[UIFont systemFontOfSize:12.f];
            label.textColor=[UIColor darkGrayColor];
            label.textAlignment=NSTextAlignmentCenter;
            
            [contentView addSubview:appButton ];
            [contentView addSubview:label];
        }
        
    }
    
    scrollView.contentSize=CGSizeMake(300, self.view.bounds.size.height+10);
    
    [scrollView addSubview:contentView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
