//
//  CDIndexViewController.m
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import "CDIndexViewController.h"
#import "CDFoundation.h"
#import "CDDirectoryTableViewController.h"
#import "CDIndexButton.h"

@implementation CDIndexViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSArray *arrayInfo=[[NSArray alloc] initWithObjects:@"新手驾驶",@"雨天驾驶",@"女性驾驶",@"雾天驾驶",@"冰雪驾驶",@"其它技巧", nil ];
    
    //1
    CDIndexButton *indexButton1=[[CDIndexButton alloc] initWithFrameText:CGRectMake(10, 10, 140, 140) LTitle:arrayInfo[0]];
    
    [indexButton1 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:indexButton1];
    
    
    
    //2
    CDIndexButton *indexButton2=[[CDIndexButton alloc] initWithFrameText:CGRectMake(170, 10, 140, 140) LTitle:arrayInfo[1]];
    
    [indexButton2 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:indexButton2];
        
    
    //3
    
    CDIndexButton *indexButton3=[[CDIndexButton alloc] initWithFrameText:CGRectMake(10, 160, 140, 140) LTitle:arrayInfo[2]];
    
    [indexButton3 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:indexButton3];
    
    
    //4
    
    CDIndexButton *indexButton4=[[CDIndexButton alloc] initWithFrameText:CGRectMake(170, 160, 140, 140) LTitle:arrayInfo[3]];
    
    [indexButton4 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:indexButton4];
        
    
    //5
    
    CDIndexButton *indexButton5=[[CDIndexButton alloc] initWithFrameText:CGRectMake(10, 310, 140, 140) LTitle:arrayInfo[4]];
    
    [indexButton5 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:indexButton5];
    
  
    
    
    //6
    CDIndexButton *indexButton6=[[CDIndexButton alloc] initWithFrameText:CGRectMake(170, 310, 140, 140) LTitle:arrayInfo[5]];
    
    [indexButton6 addTarget:self action:@selector(onClickDirectory:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:indexButton6];
    
}


-(void)onClickDirectory:(UIButton *) sender
{
    
    //NSLog(@"%@",sender.titleLabel.text);
    
    
    CDDirectoryTableViewController *directoryTableViewController=[[CDDirectoryTableViewController alloc] init ];
    
    directoryTableViewController.keystring=sender.titleLabel.text;
    
    
    [self.navigationController pushViewController:directoryTableViewController animated:YES ];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

@end
