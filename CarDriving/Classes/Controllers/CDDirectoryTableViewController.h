//
//  CDDirectoryTableViewController.h
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDDirectoryTableViewController : UITableViewController
{
    NSInteger pointsValue;

}

@property(nonatomic,copy)NSString *keystring;
@property(nonatomic,strong)NSArray *arrTitle;

//@property(nonatomic,strong)UITableView *tableViewDirectory;

@end
