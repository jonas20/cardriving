//
//  CDFoundation.h
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//



//免费版评价
#define AppStoreReview @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=650739449"

//专业版评价
#define AppStoreProReview @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=650740875"


//app url 免费版
#define AppStoreURL @"https://itunes.apple.com/cn/app/qi-che-jia-shi/id650739449?ls=1&mt=8"

//app url 专业版
#define AppStoreURLPro @"https://itunes.apple.com/cn/app/qi-che-jia-shipro/id650740875?ls=1&mt=8"



#ifndef CarDriving_CDFoundation_h
#define CarDriving_CDFoundation_h

//判断是不是iphone5设备
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)


#define UTColor(r,g,b,a)[UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

//首页button文字背影颜色
#define textBackgroundColor UTColor(52,184,222,1)

//按住首页button文字背影颜色
#define textanBackgroundColor UTColor(0,75,96,1)

//view 背景
#define viewBackgroundColor UTColor(251,251,251,1)

#define Color_blackColor UTColor(55,55,5,1)
#define Color_BackgroundColor UTColor(253,253,253,1)
#define Color_aboutTableBackground UTColor(250,250,250,1)


//视图高度 去掉导航视图和tab
#define ViewHeight [UIScreen mainScreen].bounds.size.height-44-20


#define isFreeVersion ([[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"] isEqualToString:@"cn.com.iresearch.cardriving"])

#endif
