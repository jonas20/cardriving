//
//  main.m
//  CarDriving
//
//  Created by jonas hu on 13-5-2.
//  Copyright (c) 2013年 jonas hu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CDAppDelegate class]));
    }
}
